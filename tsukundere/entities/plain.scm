;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere entities plain)
  #:use-module ((baka text) #:prefix baka:)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module ((sdl2) #:select (make-color))
  #:use-module (sdl2 render)
  #:use-module (tsukundere components)
  #:use-module (tsukundere components text)
  #:use-module (tsukundere components textures)
  #:use-module (tsukundere entities)
  #:use-module ((tsukundere game internals) #:select (current-renderer))
  #:use-module (tsukundere math)
  #:replace (angle)
  #:export (position
            anchor
            render-function render visible?
            texture render-texture-entity
            surface->entity texture->entity
            make-text
            make-layer (%layer . layer) render-layer entity-at layer-at
            layer-append! layer-cons! layer-clear! layer-delete! layer-replace!
            %layer-append! ; for use in menus
            make-scene scene-add! scene-clear! scene-delete!))

(define-component position *registry* 'position)
(define-component anchor *registry* 'anchor)
(define-component angle *registry* 'angle 0)
(define-component render-function *registry* 'render)
(define-component visible? *registry* 'visible? #t)

(define (render entity)
  (when (visible? entity)
    ((render-function entity) entity)))

(define texture (component->procedure *registry* 'texture))
(define (render-texture-entity entity)
  (%render-texture (texture entity)
                   (position entity)
                   (anchor->procedure (anchor entity))
                   (angle entity)))

;; Convert @var{surface} into an entity named @var{name}.
;; Use @var{position} and @var{anchor} to position the returned entity.
(define* (surface->entity surface #:key name
                          (position #s32(0 0)) (anchor 'center))
  (texture->entity (surface->texture (current-renderer) surface)
                   #:position position #:anchor anchor #:name name))

;; Convert @var{texture} into an entity named @var{name}.
;; Use @var{position} and @var{anchor} to position the returned entity.
(define* (texture->entity texture #:key name
                          (position #s32(0 0))
                          (angle 0)
                          (anchor 'center))
  (let ((e (make-entity name)))
    (entity-set! *registry* e 'texture texture)
    (entity-set! *registry* e 'position position)
    (entity-set! *registry* e 'angle angle)
    (entity-set! *registry* e 'anchor anchor)
    (entity-set! *registry* e 'render render-texture-entity)
    e))

;; Create a new text entity with dimensions @var{dimensions}, rendering
;; @var{string} in the font @var{font} at @var{position} aligned according to
;; @var{alignment}.
;; Fill the text using @var{fill-color} and stroke a border of width
;; @var{stroke-width} using @var{stroke-color}.
;; When @var{num-revealed} is given, only reveal that many characters.
;; When @var{markup?} is truthy, parse @var{string} as markup first.
(define* (make-text dimensions
                    #:optional
                    (string "")
                    #:key
                    name
                    markup?
                    (anchor 'top-left)
                    (angle 0)
                    (font %default-font)
                    (spacing (list 0.5 12))
                    (position #s32(0 0))
                    (fill-color (make-color 255 255 255 255))
                    (stroke-color (make-color 0 0 0 255))
                    (stroke-width 1.0)
                    num-revealed
                    (alignment '(left  . top)))
  (let ((e (make-entity name))
        (t (baka:make-text-field #:fill-rgba fill-color
                                 #:stroke-rgba stroke-color
                                 #:stroke-width stroke-width))
        (%texture (make-streaming-texture baka:%text-pixel-format
                                          (array-ref dimensions 0)
                                          (array-ref dimensions 1))))
    (entity-set! *registry* e 'position position)
    (entity-set! *registry* e 'anchor anchor)
    (entity-set! *registry* e 'angle angle)

    (if markup?
        (set! (baka:text-markup t) string)
        (set! (baka:text-content t) string))
    (set! (baka:text-font t) font)
    (set! (baka:text-alignment t) alignment)
    (set! (baka:text-spacing t) spacing)

    (if num-revealed
        (set! (baka:text-num-revealed t) num-revealed)
        (set! (baka:text-num-revealed t)
              (string-length (baka:text-content t))))

    (set! (text-field e) t)
    (set! (text-length e) (string-length string))

    (baka:paint! %texture t)
    (set! (render-function e) render-texture-entity)
    ;; For compatibility with transitions.
    ;; Do not try to mess with this texture otherwise.
    (set! (texture e) %texture)
    e))

(define-component %layer *registry* 'layer)
(define render-layer (compose (cute for-each render <>) %layer))

;; Return a new layer named @var{name} containing @var{entities}.
(define (make-layer name . entities)
  (let ((e (make-entity name)))
    (set! (%layer e) entities)
    (set! (render-function e) render-layer)
    e))

;; Return a scene with a layer for each element of @var{layers}.
;; Since layer names are compared using @code{eq?}, @var{layers} should be
;; a list of symbols.
(define (make-scene layers)
  (apply make-layer '*top* (map make-layer layers)))

(define (entity-name=? entity name)
  (match entity
    (($ <entity> ename) (eq? name ename))
    (_ #f)))

;; @deffnx procedure layer-at top . path
;; Find an entity or layer respectively by starting from @var{top} and
;; constructing a way, down always picking the entity whose name is @code{eq?}
;; to the respective element of @var{path}.
;; @example
;; ;; find the speaker entity in the text layer
;; (entity-at *top* 'text 'speaker)
;; ;; get the entities of the foreground layer as list
;; (layer-at *top* 'foreground)
;; @end example
(define (entity-at top . path)
  (let %entity-at ((e0 top)
                   (path path))
    (and e0
         (match path
           (() e0)
           ((part . rest)
            (%entity-at
             (find
              (cute entity-name=? <> part)
              (if (list? e0) e0 (%layer e0)))
             rest))))))

(define layer-at (compose (cute and=> <> %layer) entity-at))

(define-inlinable (%layer-append! layer entities)
  (set! (%layer layer) (append (%layer layer) entities)))

;; Add @var{entities} to the back of @var{layer}.
(define (layer-append! layer . entities)
  (%layer-append! layer entities))

;; Add @var{entity} to the front of @var{layer}.
(define (layer-cons! layer entity)
  (set! (%layer layer) (cons entity (%layer layer))))

;; Remove @var{entity} from @var{layer}.
;; @var{entity} can either be an entity record or a symbol.
;; In the case of the former, @var{eq?} is used to compare entities to it,
;; in the case of the latter, the entities' names are compared to it.
(define (layer-delete! layer entity)
  (set! (%layer layer) (delete entity (%layer layer)
                               (if (symbol? entity) entity-name=? eq?))))

;; Replace @var{entity} in @var{layer} with @var{replacement}.
;; @var{entity} can either be an entity record or a symbol.
;; In the case of the former, @var{eq?} is used to compare entities to it,
;; in the case of the latter, the entities' names are compared to it.
(define (layer-replace! layer entity replacement)
  (set! (%layer layer)
    (map
     (if (symbol? entity)
         (lambda (e2) (if (entity-name=? e2 entity) replacement e2))
         (lambda (e2) (if (eq? e2 entity) replacement e2)))
     (%layer layer))))

;; Remove all entities from @var{layer}.
(define (layer-clear! layer)
  (set! (%layer layer) '()))

;; Clear layer @var{layer} in @var{scene}.
(define (scene-clear! scene layer)
  (layer-clear! (entity-at scene layer)))

;; Add @var{entities} to @var{layer} in @var{scene}.
(define (scene-add! scene layer . entities)
  (%layer-append! (entity-at scene layer) entities))

;; Remove @var{entity} from @var{layer} in @var{scene}.
(define (scene-delete! scene layer entity)
  (layer-delete! (entity-at scene layer) entity))
