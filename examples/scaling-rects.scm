;; Copyright © 2020 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; Copying and distribution of this file, with or without modification,
;; are permitted in any medium without royalty provided the copyright
;; notice and this notice are preserved.  This file is offered as-is,
;; without any warranty.

(use-modules (tsukundere)
             (sdl2 rect)
             (sdl2 render))

(define (load)
  (let ((rects #2s32((0 0 100 100)
                     (100 100 100 100)
                     (200 200 100 100)
                     (300 300 100 100)))
        (scene (make-scene '(dot)))
        (ent (make-entity)))
    (set! (render-function ent)
          (lambda _
            (set-renderer-draw-color! (current-renderer) 255 255 255 255)
            (fill-rects (current-renderer) (array-contents rects))
            (set-renderer-draw-color! (current-renderer) 0 0 0 255)))
    (scene-add! scene 'dot ent)
    (init-script! (lambda _ (pause)) scene)))

(run-game
 #:game-name ".tsukundere-example-scaling-rects"
 #:window-width 400
 #:window-height 400
 #:init! load)
