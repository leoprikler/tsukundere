;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere components)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:export (<registry>
            *registry*
            make-registry unwrap-registry
            registry-add-component! define-component component->procedure))

(define <registry> (make-record-type '<registry> '(components)))

(define make-registry
  (compose
   (record-constructor <registry>)
   make-hash-table))

(define *registry* (make-registry))

(define unwrap-registry (record-accessor <registry> 'components))

;; Return the component named @var{component}, a symbol, in @var{registry}
;; or create a new one and return the underlying hash table.
(define (registry-add-component! registry component)
  (or (hashq-ref (unwrap-registry registry) component)
      (let ((table (make-weak-key-hash-table)))
        (hashq-set! (unwrap-registry registry) component table)
        table)))

;; Return a procedure, that wraps the component named @var{component} in
;; @var{registry}.
;; The returned procedure can be used both as a getter and a setter and takes
;; an entity as argument.
(define* (component->procedure registry component #:optional default)
  (let ((table (hashq-ref (unwrap-registry registry) component)))
    (make-procedure-with-setter (cute hashq-ref table <> default)
                                (cute hashq-set! table <> <>))))

(define-syntax define-component
  (syntax-rules ()
    ((_ var registry value)
     (define-component var registry value #f))
    ((_ var registry value default)
     (define var
       (let ((table (registry-add-component! registry value)))
         (make-procedure-with-setter
          (cute hashq-ref table <> default)
          (cute hashq-set! table <> <>)))))))
