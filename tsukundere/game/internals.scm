;; Copyright © 2020, 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere game internals)
  #:use-module (ice-9 q)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere script)
  #:export (*game-agenda*
            *script-stack*
            *window*
            *corners*
            *renderer*
            *game-name*
            *event-handler*
            *save-state*

            quit-prompt
            current-window current-renderer current-game
            current-script current-scene current-event-handler
            %top-left %top-right %bottom-left %bottom-right %center))

(define *game-agenda* (make-agenda))

(define *script-stack* (make-q))
(define *window* (make-unbound-fluid))
(define *corners* (make-unbound-fluid))
(define *renderer* (make-unbound-fluid))
(define *game-name* (make-unbound-fluid))

(define *save-state* (make-unbound-fluid))

(define-inlinable (current-window)
  "Return the currently active window or #f."
  (and (fluid-bound? *window*) (fluid-ref *window*)))
(define (current-renderer)
  "Return the current renderer.
Unlike current-window, don't check for errors.  It is an error to call this
function before before init!"
  (fluid-ref *renderer*))
(define-inlinable (current-game)
  "Return the name of the current game or #f."
  (and (fluid-bound? *game-name*) (fluid-ref *game-name*)))

(define (current-script)
  (and (not (q-empty? *script-stack*))
       (caar *script-stack*)))
(define (current-scene)
  (and=> (current-script) script-scene))
(define (current-event-handler)
  (and=> (current-script) script-event-handler))

(define quit-prompt (make-prompt-tag))

(define-inlinable (%top-left)     (array-slice (fluid-ref *corners*) 0))
(define-inlinable (%top-right)    (array-slice (fluid-ref *corners*) 1))
(define-inlinable (%bottom-left)  (array-slice (fluid-ref *corners*) 2))
(define-inlinable (%bottom-right) (array-slice (fluid-ref *corners*) 3))
(define-inlinable (%center)       (array-slice (fluid-ref *corners*) 4))
