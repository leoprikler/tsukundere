;; Copyright © 2020, 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere game)
  #:use-module (sdl2)
  #:use-module ((sdl2 image) #:select ((load-image . load-image/surface)))
  #:use-module (sdl2 hints)
  #:use-module (sdl2 mixer)
  #:use-module (sdl2 render)
  #:use-module (sdl2 surface)
  #:use-module (sdl2 video)
  #:use-module (system foreign)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere assets)
  #:use-module (tsukundere components text)
  #:use-module (tsukundere entities)
  #:use-module (tsukundere entities debug)
  #:use-module (tsukundere entities plain)
  #:use-module (tsukundere entities menu)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere game modules)
  #:use-module (tsukundere history)
  #:use-module (tsukundere math)
  #:use-module (tsukundere preferences)
  #:use-module (tsukundere script)
  #:use-module (tsukundere script events)
  #:use-module (tsukundere sound)
  #:use-module (tsukundere utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 q)

  #:re-export (current-window
               current-renderer
               current-script current-scene)

  #:export (run-game
            run-game/module
            call/paused
            init-script! init-text!
            init-menu! step! skip!))

(define *fullscreen?* (make-preference 'fullscreen? #f #:validate? boolean?))
(add-preference-hook!
 *fullscreen?*
 (lambda (value)
   (and-let* ((window (current-window)))
     (set-window-fullscreen! window value #:desktop? value))))
(define fullscreen? (preference->procedure *fullscreen?*))

(define (is-running?)
  "Return #t if a script is currently running, otherwise #f."
  (and=> (current-script)
         (lambda (script) (eq? (script-status script) 'running))))

;; Set up the main script of the game to execute @var{thunk}, also setting the
;; scene to @var{scene}.  While this function does not barf if called multiple
;; times, it really should only be called once.
(define* (init-script! thunk #:optional (scene (make-scene '())))
  "Initialize the script library, setting THUNK as script and SCENE as scene.
THUNK is not executed until start! is called."
  (set-car! *script-stack*
            (list
             (make-script
              thunk
              scene
              (event-handler #:key-press key-press
                             #:key-release key-release
                             #:mouse-press mouse-press
                             #:mouse-release mouse-release
                             #:mouse-move mouse-move
                             #:text-input text-input))))
  (sync-q! *script-stack*))

;; Initialize the @code{text} layer of @var{scene} with a text entity
;; of dimensions @var{speaker-dimensions} at position @var{speaker-position}
;; being used for the speaker and a text object of dimensions
;; @var{text-dimensions} at position @var{text-position} being used for
;; dialogue.
;; Additionally pass @var{text-options} and @var{speaker-options}, a list of
;; keyword-value pairs to the initializers of each text entity respectively.
(define* (init-text! text-position text-dimensions
                     speaker-position speaker-dimensions
                     #:key (scene (current-scene))
                     text-options speaker-options)
  "Initialize speaker and text objects at (X SPEAKER_Y) and (X TEXT_Y)
in the 'text layer using SPEAKER_FONT and TEXT_FONT respectively."
  (set! (layer (entity-at scene 'text))
    (list
     (apply make-text speaker-dimensions
            ""
            #:name 'speaker
            #:position speaker-position
            speaker-options)
     (apply make-text text-dimensions
            ""
            #:name 'text
            #:position text-position
            text-options))))

;; Add @var{menu} to the @code{menu} layer of @var{scene}.  If menu is
;; not provided, a new one is constructed from the key parameters, which
;; are passed to @var{make-menu-layer}.
;; It is an error to pass both @var{menu} and key parameters.
(define* (init-menu! #:optional menu
                     #:key (scene (current-scene))
                     position angle spacing
                     button accumulator validator button-callback
                     text-font text-alignment text-offset
                     text-spacing text-stroke-width)
  (layer-replace!
   scene (if menu (entity-name menu) 'menu)
   (or menu
       (make-menu-layer #:position position
                        #:angle angle
                        #:spacing spacing
                        #:button button
                        #:accumulator accumulator
                        #:validator validator
                        #:button-callback button-callback
                        #:text-font text-font
                        #:text-alignment text-alignment
                        #:text-offset text-offset
                        #:text-spacing text-spacing
                        #:text-stroke-width text-stroke-width))))

(define (update delta)
  "Update the script clock and advance the text."
  (when (is-running?)
    (if (script-skipping? (current-script))
        (skip!)
        (update-agenda! (script-clock (current-script)) delta))
    (match (layer-at (current-scene) 'text)
      (#f *unspecified*)
      (layer
       (and-let* ((text (entity-at layer 'text)))
         (text-advance! text delta))))))
(schedule-plan! *game-agenda* update)

;; Start the script.  If @var{pos} is given, it is a list
;; (@var{n1} @var{n2} ...), in which @var{n1 ...} lines are skipped in order
;; to get to the next script.
(define* (start! #:key pos)
  "Start the script, optionally skipping LINES, a number of lines."
  (let ((initial-script
         (fresh-script
          (fold (lambda (cur seed)
                  (set-script-status! cur 'cancelled)
                  (run-hook (script-restore cur))
                  (reset-hook! (script-restore cur))
                  cur)
                *unspecified*
                (car *script-stack*)))))
    (set-car! *script-stack* (list initial-script))
    (sync-q! *script-stack*)
    (start-script (current-script))
    (when pos
      (fold
       (lambda (n script)
         (skip! script n)
         (current-script))
       initial-script
       pos))))

(add-load-hook!
 (lambda* (#:key tsukundere-position #:allow-other-keys)
   (start! #:pos tsukundere-position)))

;; Step forward through the current script.
;; If the scene contains text, first ensure, that it is fully readable.
(define (step!)
  (let ((script (current-script)))
    (unless (speed-through-clock! script #t)
      (match (layer-at (script-scene script) 'text)
        (#f (forward-line! script))
        (%layer
         (unless (and=> (entity-at %layer 'text) text-reveal-all!)
           (forward-line! script)))))))

;; Skip @var{n} lines of @var{script}.
;; If @var{n} is not given, attempt to skip one line, but stop early when
;; @var{script} appears to no longer be skipping.
(define* (skip! #:optional (script (current-script)) n)
  (cond
   ((not n)
    (when (script-skipping? script) (speed-through-clock! script))
    (when (script-skipping? script) (forward-line! script))
    (when (script-skipping? script)
      (match (layer-at (script-scene script) 'text)
        (#f *unspecified*)
        (%layer
         (and=> (entity-at %layer 'text) text-reveal-all!)
         *unspecified*))))
   ((> n 0)
    (speed-through-clock! script #t)
    (forward-line! script)
    (skip! script (1- n)))
   ((= n 0)
    (speed-through-clock! script #t)
    (match (layer-at (script-scene script) 'text)
      (#f *unspecified*)
      (%layer
       (and=> (entity-at %layer 'text) text-reveal-all!)
       *unspecified*)))))

(define (key-press key code mod repeat?)
  (define non-null? (negate null?))
  (match (list key repeat?)
    ((_ #t) *unspecified*)
    (('return _) (step!))
    (('space _) (step!))
    (('left-control _) (set-script-skipping! (current-script) #t))
    (('right-control _) (set-script-skipping! (current-script) #t))
    (((? integer? n) _)
     (if (non-null? (lset-intersection eq? mod '(left-shift right-shift)))
         (save (current-game) n)
         (load (current-game) n)))
    (('f4 _)
     (and=> (entity-at (current-scene) 'debug 'fps-counter)
            (lambda (counter)
              (if (visible? counter) ; assume visible means active
                  (deactivate-fps-counter! counter)
                  (activate-fps-counter! counter)))))
    (('f11 _)
     (set! (fullscreen? '*game*) (not (fullscreen?))))
    (('h _)
     (match (entity-at (current-scene) 'text)
       (#f *unspecified*)
       (text (set! (visible? text) (not (visible? text))))))
    ((_ _) *unspecified*)))

(define (key-release key code mod)
  (case key
    ((left-control right-control)
     (set-script-skipping! (current-script) #f))
    (else *unspecified*)))

(define (mouse-press button clicks pos)
  (match (list button)
    (('left) (step!))
    ((_) *unspecified*)))

(define mouse-release (const *unspecified*))
(define mouse-move (const *unspecified*))
(define text-input (const *unspecified*))

;; Call @var{thunk} as a new script while pausing the current one.
;; @var{key-press}, @var{key-release}, @var{mouse-press}, @var{mouse-release},
;; @var{mouse-move} and @var{input} are passed on to @code{event-handler} to
;; form an event handler, that is used while @var{thunk} still runs.
;; Likewise, @var{scene} will be rendered while @var{thunk} is active.
(define* (call/paused thunk
                      #:key
                      (scene (script-scene (current-script)))
                      (hard-pause? #f)
                      (key-press (on-key-press (current-event-handler)))
                      (key-release(on-key-release (current-event-handler)))
                      (mouse-press (on-mouse-press (current-event-handler)))
                      (mouse-release (on-mouse-release (current-event-handler)))
                      (mouse-move (on-mouse-move (current-event-handler)))
                      (text-input (on-text-input (current-event-handler))))
  (pause/script (make-script thunk scene
                             (event-handler #:key-press key-press
                                            #:key-release key-release
                                            #:mouse-press mouse-press
                                            #:mouse-release mouse-release
                                            #:mouse-move mouse-move
                                            #:text-input text-input))
                hard-pause?))

(define (init-noop)
  (init-script! (lambda () (pause))))

;; Run the game @var{game-name}, which is initialized via @var{init!}.
;; @*
;; Use @var{time} to keep track of the current time, updating the game
;; state and rendering every @code{(/ 1 update-freq)} seconds.
;; @*
;; Use @var{window-title} as the title of the window @var{game-name} runs
;; in and initalize its size using @var{window-width} and @var{window-height}.
;; If @var{window-icon} is given, interpret it as the name of an image file
;; to set as window icon.
;; @*
;; If @var{use-mixer?} is @code{#t}, also initialize SDL mixer for sound
;; support, using @var{mixer-formats} for the supported formats in
;; @code{mixer-init},  and @var{mixer-frequency} and @var{mixer-chunk-size}
;; to set the @code{#:frequency} and @code{chunk-size} arguments in
;; @code{open-audio} respectively.
;; @*
;; When the player tries to save, call @var{save} to produce a list of
;; @code{#:variable value} assignments, that will be saved along with all
;; the internal variables.
(define* (run-game #:key
                   (game-name (basename (car (program-arguments))))
                   (init! init-noop)

                   locale-dir

                   (time sdl-ticks)
                   (update-freq 60)

                   (window-title "Tsukundere")
                   (window-width 600)
                   (window-height 480)
                   (window-icon #f)

                   (use-mixer? #t)
                   (mixer-formats '(flac mp3 ogg))
                   (mixer-frequency 44100)
                   (mixer-chunk-size 1024)

                   (save (const '())))

  ;; Let's be extra careful to avoid path traversal
  (let ((game-name (and=> game-name basename)))
    (unless (string=? game-name "tsukundere")
      (fluid-set! *game-name* game-name)
      (when locale-dir
        (bindtextdomain game-name locale-dir))))

  (load-preferences)
  (save-preferences #f 'unless-exists?)
  (and=> (current-game) load-preferences)

  (fluid-set! *save-state* save)
  (sdl-init)
  (set-hint! 'render-scale-quality 2)
  (when use-mixer?
    (init-sound! #:mixer-formats mixer-formats
                 #:mixer-frequency mixer-frequency
                 #:mixer-chunk-size mixer-chunk-size))
  (call-with-window (make-window #:title window-title
                                 #:size (list window-width window-height)
                                 #:resizable? #t
                                 #:fullscreen-desktop? (fullscreen?)
                                 #:show? #f)
    (lambda (window)
      (set-window-minimum-size! window window-width window-height)
      (let ((corners (make-typed-array 's32 0 5 2)))
        (array-set! corners window-width  1 0)
        (array-set! corners window-height 2 1)
        (array-set! corners window-width  3 0)
        (array-set! corners window-height 3 1)
        (array-set! corners (floor/ window-width 2)  4 0)
        (array-set! corners (floor/ window-height 2) 4 1)
        (fluid-set! *corners* corners)
        (fluid-set! *window* window))
      (let ((icon (and=> window-icon expand-asset-path)))
        (when (and=> icon file-exists?)
          (call-with-surface (load-image/surface icon)
                             (cute set-window-icon! window <>))))
      (call-with-renderer (make-renderer window)
        (lambda (renderer)
          (show-window! window)
          (fluid-set! *renderer* renderer)
          (fit-current-window!)
          (init!)
          (start!)
          (call-with-prompt quit-prompt
            (lambda ()
              (let loop ((delta (/ 1000 update-freq))
                         (last (time)))
                (let ((now (time)))
                  (handle-events)
                  (update-agenda! *game-agenda* (- now last))
                  (clear-renderer (current-renderer))
                  (render (current-scene))
                  (present-renderer (current-renderer))
                  (usleep (floor delta))
                  (loop delta now))))
            (lambda _
              (and=> (current-game) save-preferences))))))))

;; Run the game defined by the variables of @var{module}.
;; If @var{module} is a module spec, resolve the public interface of that
;; module first and use it instead.
(define (run-game/module module . args)
  (apply run-game
         (module->keyword-list (match module
                                 ((? module? module) module)
                                 ((and (? list spec)
                                       (? (cute every symbol? <>)))
                                  (resolve-interface* spec)))
                               args)))
