;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere cli check)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 pretty-print)
  #:use-module (sdl2)
  #:use-module (sdl2 render)
  #:use-module (sdl2 video)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (tsukundere assets)
  #:use-module (tsukundere cli)
  #:use-module (tsukundere i18n)
  #:use-module (tsukundere components text)
  #:use-module (tsukundere entities plain)
  #:use-module (tsukundere game)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere game modules)
  #:use-module ((tsukundere history) #:prefix history:)
  #:use-module (tsukundere script)
  #:use-module (tsukundere sound)
  #:use-module (tsukundere utils)
  #:export (*options* *values* *help*
            check))

(define text-string (compose (@ (baka text) text-content) text-field))

(define *options*
  (cons* (option '(#\L "load-path") #t #f
                 (lambda (opt name arg result)
                   (acons 'load-path (cons arg (assoc-ref result 'load-path))
                          result)))
         (option '(#\A "asset-dir") #t #f
                 (lambda (opt name arg result)
                   (if (assoc-ref result 'asset-dir)
                       (fail "`-A' option can not be specified more than once")
                       (acons 'asset-dir arg result))))
         (option '(#\m "module") #t #f
                 (lambda (opt name arg result)
                   (if (assoc-ref result 'module)
                       (fail "`-m' option can not be specified more than once")
                       (acons 'module
                              (map string->symbol (string-split arg #\space))
                              result))))
         base-options))

(define *values*
  '((load-path)
    (files)))

(define (*help*)
  (T_ "Usage: tsukundere check [FILE]...
Check whether a visual novel using the Tsukundere engine correctly loads a
given save file.

  -h, --help            print this help message
  -A, --asset-dir=DIR   set DIR as the directory, from which assets are loaded
  -L, --load-path=DIR   add DIR to the front of the module load path
  -m, --module=MODULE   use MODULE as the entry point (required)"))

(define* (check #:key
                help? asset-dir (load-path '()) module files)
  (when help? (show-help "check"))
  (unless module
    (fail "no module given"))
  (when (null? files)
    (fail "no save file given"))

  (when asset-dir (set-asset-path! asset-dir))

  (set! %load-path (append load-path %load-path))
  (set! %load-should-auto-compile #f)

  (apply check-1 (reverse files)
         (module->keyword-list (resolve-interface* module))))

(define* (check-1 save-files
                  #:key
                  init!

                  (window-title "Tsukundere")
                  (window-width 600)
                  (window-height 480)

                  (use-mixer? #t)
                  (mixer-formats '(flac mp3 ogg))
                  (mixer-frequency 44100)
                  (mixer-chunk-size 1024)

                  #:allow-other-keys)
  (unless init!
    (fail "game does not provide an init! method"))
  ;; Don't forget to update this procedure as run-game changes.
  (define stats '())
  (define times (make-vector 4 #f))
  (display (G_ "initializing Tsukundere…") (current-warning-port))
  (vector-set! times 0 (get-internal-real-time))
  (sdl-init)
  (when use-mixer?
    (init-sound! #:mixer-formats mixer-formats
                 #:mixer-frequency mixer-frequency
                 #:mixer-chunk-size mixer-chunk-size))
  (set! (sound-volume) 0)
  (set! (music-volume) 0)
  (call-with-window (make-window #:title window-title
                                 #:size (list window-width window-height)
                                 #:show? #f)
    (lambda (window)
      (set-window-minimum-size! window window-width window-height)
      (let ((corners (make-typed-array 's32 0 5 2)))
        (array-set! corners window-width  1 0)
        (array-set! corners window-height 2 1)
        (array-set! corners window-width  3 0)
        (array-set! corners window-height 3 1)
        (array-set! corners (floor/ window-width 2)  4 0)
        (array-set! corners (floor/ window-height 2) 4 1)
        (fluid-set! *corners* corners)
        (fluid-set! *window* window))
      (call-with-renderer (make-renderer window)
        (lambda (renderer)
          (fluid-set! *renderer* renderer)
          (vector-set! times 1 (get-internal-real-time))
          (display (G_ " done.\n") (current-warning-port))
          (display (G_ "initializing game…") (current-warning-port))
          (vector-set! times 2 (get-internal-real-time))
          (init!)
          (vector-set! times 3 (get-internal-real-time))
          (display (G_ " done.\n") (current-warning-port))
          (pretty-print
           (reverse
            (let loop ((save-files save-files)
                       (result
                        `((game-load
                           ,(exact->inexact
                             (/ (- (vector-ref times 3) (vector-ref times 2))
                                internal-time-units-per-second)))
                          (check-init
                           ,(exact->inexact
                             (/ (- (vector-ref times 1) (vector-ref times 0))
                                internal-time-units-per-second))))))
              (match save-files
                (() result)
                ((save-file . rest)
                 (format (current-warning-port) (G_ "loading save ~a…") save-file)
                 (vector-set! times 0 (get-internal-real-time))
                 (if (call-with-prompt quit-prompt
                       (lambda ()
                         (history:load-from-state
                          (call-with-input-file save-file read))
                         (vector-set! times 1 (get-internal-real-time))
                         (display (G_ " done.\n") (current-warning-port))
                         #t)
                       (lambda _
                         (vector-set! times 1 (get-internal-real-time))
                         (display (G_ " end reached.\n") (current-warning-port))
                         #f))
                     (loop rest (check-report save-file times result))
                     (check-report save-file times result))))))))))))

(define (check-report save-file times result)
  (cons
   `(check
     ,save-file
     (state
      ,(reverse
        (map (lambda (script)
               (vector (script-line script)
                       (script-status script)))
             (car *script-stack*))))
     (dialogue
      ,@(match (and (and=> (current-script)
                           (compose (cute eq? 'running <>)
                                    script-status))
                    (layer-at (current-scene) 'text))
          (#f '(*unspecified*))
          (layer
           (list (text-string (entity-at layer 'speaker))
                 (text-string (entity-at layer 'text))))))
     (time
      ,(exact->inexact
        (/ (- (vector-ref times 1) (vector-ref times 0))
           internal-time-units-per-second))))
   result))
