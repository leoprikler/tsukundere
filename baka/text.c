/* Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <SDL.h>
#include <fontconfig/fontconfig.h>
#include <pango/pangocairo.h>
#include <libguile.h>

/**
 * Undo premultiplication done on @var{pixels}, an array of @var{height}
 * rows with @var{pitch} bytes per row.
 */
static void
postdivide (guint8 *pixels, gint height, gint pitch)
{
  guint8 *end = pixels + height * pitch;

  for (guint32 *c = (guint32 *)pixels; c < (guint32 *)end; c++)
    {
      guint32
        a = (*c >> 24) & 0xff,
        r = (*c >> 16) & 0xff,
        g = (*c >>  8) & 0xff,
        b = (*c >>  0) & 0xff;

      if (a == 0) continue;

      *c =
        (a          ) << 24 |
        (r * 255 / a) << 16 |
        (g * 255 / a) <<  8 |
        (b * 255 / a);
    }
}

static void
pango_rectangle_from_sdl (PangoRectangle *dst,
                          const SDL_Rect *src)
{
  dst->x      = pango_units_from_double(src->x);
  dst->y      = pango_units_from_double(src->y);
  dst->width  = pango_units_from_double(src->w);
  dst->height = pango_units_from_double(src->h);
}

typedef enum _BakaVerticalAlignment {
  BAKA_VALIGN_TOP = 0,
  BAKA_VALIGN_CENTER,
  BAKA_VALIGN_BOTTOM
} BakaVerticalAlignment;

typedef struct _BakaPaintOptions
{
  gint                  n_revealed;
  BakaVerticalAlignment valign;
  gdouble               fill_rgba[4];
  gdouble               stroke_rgba[4];
  gdouble               stroke_width;
} BakaPaintOptions;

static SCM sym_left, sym_center, sym_right;

static void
pango_attr_list__finalize (gpointer attrs)
{
  pango_attr_list_unref ((PangoAttrList *) attrs);
}

static void
baka_text_draw_glyph_item (cairo_t *cr,
                           PangoGlyphItem *run,
                           BakaPaintOptions *options)
{
  g_return_if_fail (run != NULL);

  gdouble fill_rgba[4], stroke_rgba[4], stroke_width;
  memcpy (fill_rgba, options->fill_rgba, sizeof (fill_rgba));
  memcpy (stroke_rgba, options->stroke_rgba, sizeof (stroke_rgba));
  stroke_width = options->stroke_width;

  for (GSList *a = run->item->analysis.extra_attrs; a != NULL; a = a->next)
    {
      PangoAttribute *attr = a->data;
      PangoColor *color;
      guint16 alpha;

      switch ((int) attr->klass->type)
        {
        case PANGO_ATTR_WEIGHT:
          {
            double weight = ((PangoAttrInt *) attr)->value;
            stroke_width = options->stroke_width * weight / PANGO_WEIGHT_NORMAL;
            break;
          }
        case PANGO_ATTR_FOREGROUND:
	  color = &((PangoAttrColor *)attr)->color;
          fill_rgba[0] = color->red / 65535.;
          fill_rgba[1] = color->green / 65535.;
          fill_rgba[2] = color->blue / 65535.;
	  break;
	case PANGO_ATTR_FOREGROUND_ALPHA:
          alpha = (guint16) ((PangoAttrInt *) attr)->value;
          fill_rgba[3] = alpha / 65535.;
	  break;
        case PANGO_ATTR_BACKGROUND:
          // We don't need to fill in any background, so we can instead abuse
          // this attribute for the stroke color.
	  color = &((PangoAttrColor *)attr)->color;
          stroke_rgba[0] = color->red / 65535.;
          stroke_rgba[1] = color->green / 65535.;
          stroke_rgba[2] = color->blue / 65535.;
	  break;
        case PANGO_ATTR_BACKGROUND_ALPHA:
          // As above.
          alpha = (guint16) ((PangoAttrInt *) attr)->value;
          stroke_rgba[3] = alpha / 65535.;
	  break;
        }
    }

  pango_cairo_glyph_string_path (cr,
                                 run->item->analysis.font,
                                 run->glyphs);

  cairo_set_source_rgba (cr,
                         fill_rgba[0],
                         fill_rgba[1],
                         fill_rgba[2],
                         fill_rgba[3]);

  cairo_fill_preserve (cr);

  cairo_set_source_rgba (cr,
                         stroke_rgba[0],
                         stroke_rgba[1],
                         stroke_rgba[2],
                         stroke_rgba[3]);

  cairo_set_line_width (cr, stroke_width);
  cairo_stroke (cr);
}

static void
baka__do_paint (cairo_t          *cr,
                PangoLayout      *layout,
                BakaPaintOptions *options,
                PangoRectangle   *size)
{
  PangoLayoutIter *iter = pango_layout_get_iter (layout);
  const char *text = pango_layout_get_text (layout);
  gint n_shown = 0;

  PangoRectangle layout_extents;
  pango_layout_get_extents(layout, NULL, &layout_extents);

  gint y_off = 0;

  if (size->height > layout_extents.height)
    switch (options->valign)
      {
      case BAKA_VALIGN_TOP:
        break;
      case BAKA_VALIGN_CENTER:
        y_off = (size->height - layout_extents.height) / 2;
        break;
      case BAKA_VALIGN_BOTTOM:
        y_off = (size->height - layout_extents.height);
        break;
      }

  do
    {
      gint x0 = 0, y0 = y_off + pango_layout_iter_get_baseline(iter);
      PangoLayoutLine *line = pango_layout_iter_get_line_readonly(iter);
      PangoRectangle logical_rect;
      pango_layout_iter_get_line_extents (iter, NULL, &logical_rect);

      switch (line->resolved_dir) {
      case PANGO_DIRECTION_RTL:
        line->runs = g_slist_reverse(line->runs);
      case PANGO_DIRECTION_LTR:
        {
          for (GSList *runs = line->runs;
               runs != NULL && n_shown < options->n_revealed;
               runs = runs->next)
            {
              PangoLayoutRun *run = (PangoLayoutRun *) runs->data;
              if (run == NULL)
                continue;

              if (run->item->analysis.flags & PANGO_ANALYSIS_FLAG_IS_ELLIPSIS)
                {
                  // We don't show this glyph item
                  // TODO: Render ellipsis instead
                  n_shown += run->item->num_chars;
                }
              else if (n_shown + run->item->num_chars > options->n_revealed)
                {
                  gint split_index_char = options->n_revealed - n_shown;
                  PangoGlyphItem *before, *after = pango_glyph_item_copy (run);
                  gchar *looking_at = g_utf8_substring (text + run->item->offset,
                                                        0,
                                                        split_index_char);
                  gint split_index = strlen (looking_at);
                  g_free(looking_at);
                  before = pango_glyph_item_split (after, text, split_index);
                  cairo_move_to (cr,
                                 pango_units_to_double (x0 + logical_rect.x),
                                 pango_units_to_double (y0 + logical_rect.y));

                  baka_text_draw_glyph_item (cr, before, options);
                  pango_glyph_item_free (before);
                  pango_glyph_item_free (after);
                  n_shown = options->n_revealed;
                }
              else
                {
                  cairo_move_to (cr,
                                 pango_units_to_double (x0 + logical_rect.x),
                                 pango_units_to_double (y0 + logical_rect.y));
                  baka_text_draw_glyph_item (cr, run, options);
                  x0 += pango_glyph_string_get_width (run->glyphs);
                  n_shown += run->item->num_chars;
                }
            }
        }
      }
    }
  while (pango_layout_iter_next_line (iter) && n_shown < options->n_revealed);
}

void
baka_paint (SDL_Texture      *texture,
            PangoLayout      *layout,
            BakaPaintOptions *options)
{
  SDL_Rect rect = {0, 0, 0, 0};
  gpointer pixels;
  gint pitch;
  cairo_surface_t *surface;

  SDL_QueryTexture(texture, NULL, NULL, &rect.w, &rect.h);
  SDL_LockTexture (texture, &rect, &pixels, &pitch);

  surface = cairo_image_surface_create_for_data (pixels,
                                                 CAIRO_FORMAT_ARGB32,
                                                 rect.w, rect.h, pitch);
  cairo_t *cr = cairo_create (surface);
  cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND);
  cairo_set_line_join (cr, CAIRO_LINE_JOIN_ROUND);

  pango_cairo_update_context (cr, pango_layout_get_context(layout));
  pango_layout_context_changed (layout);
  pango_layout_set_width (layout, pango_units_from_double(rect.w));
  pango_layout_set_height (layout, pango_units_from_double(rect.h));

  PangoRectangle pango_rect;
  pango_rectangle_from_sdl (&pango_rect, &rect);

  memset (pixels, 0, rect.h * pitch);
  baka__do_paint (cr, layout, options, &pango_rect);
  postdivide (pixels, rect.h, pitch);

  SDL_UnlockTexture (texture);
  cairo_surface_destroy (surface);
  cairo_destroy (cr);
}

static SCM
scm_baka_make_layout ()
{
  cairo_surface_t *surface;
  surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 0, 0);

  cairo_t *cr = cairo_create (surface);
  PangoLayout *layout = pango_cairo_create_layout (cr);
  cairo_surface_destroy (surface);
  cairo_destroy (cr);
  return scm_from_pointer (layout, g_object_unref);
}

static SCM
scm_baka_layout_get_text (SCM scm_layout)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  const gchar *text = pango_layout_get_text (layout);
  scm_remember_upto_here_1 (scm_layout);
  return scm_from_utf8_string (text);
}

static SCM
scm_baka_layout_get_attributes (SCM scm_layout)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  PangoAttrList *attrs = pango_layout_get_attributes (layout);
  SCM ret = scm_from_pointer (attrs, pango_attr_list__finalize);
  scm_remember_upto_here_1 (scm_layout);
  return ret;
}

static SCM
scm_baka_layout_get_font (SCM scm_layout)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  const PangoFontDescription *font;
  font = pango_layout_get_font_description (layout);
  gchar *font_str = pango_font_description_to_string (font);
  SCM ret = scm_from_utf8_string (font_str);
  g_free (font_str);
  scm_remember_upto_here_1 (scm_layout);
  return ret;
}

static SCM
scm_baka_layout_get_alignment (SCM scm_layout)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  SCM ret;
  switch (pango_layout_get_alignment (layout))
    {
    case PANGO_ALIGN_LEFT:
      ret = sym_left;
      break;
    case PANGO_ALIGN_CENTER:
      ret = sym_center;
      break;
    case PANGO_ALIGN_RIGHT:
      ret = sym_right;
      break;
    default:
      ret = SCM_UNDEFINED;
      g_assert_not_reached ();
    }
  scm_remember_upto_here_1 (scm_layout);
  return ret;
}

static SCM
scm_baka_layout_get_spacing (SCM scm_layout)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  SCM spacing[2];
  double tmp;
#if PANGO_VERSION_CHECK(1, 44, 0)
  tmp = pango_layout_get_line_spacing (layout);
  spacing[0] = scm_from_double (tmp);
#else
  spacing[0] = scm_from_double (0.0);
#endif
  tmp = pango_layout_get_spacing (layout);
  spacing[1] = scm_from_double (tmp);
  scm_remember_upto_here_1 (scm_layout);
  return scm_c_values (spacing, 2);
}

static SCM
scm_baka_layout_set_text_x (SCM scm_layout,
                            SCM scm_text)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  gchar *text = scm_to_utf8_string (scm_text);
  pango_layout_set_text (layout, text, -1);
  free (text);
  pango_layout_set_attributes (layout, NULL);
  scm_remember_upto_here_1 (scm_layout);
  return SCM_UNSPECIFIED;
}

static SCM
scm_baka_layout_set_attributes_x (SCM scm_layout,
                                  SCM scm_attrs)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  PangoAttrList *attrs = scm_to_pointer (scm_attrs);
  pango_layout_set_attributes (layout, attrs);
  scm_remember_upto_here_2 (scm_layout, scm_attrs);
  return SCM_UNSPECIFIED;
}

static SCM
scm_baka_layout_set_font_x (SCM scm_layout, SCM scm_font)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  gchar *font_str = scm_to_utf8_string (scm_font);
  PangoFontDescription *font = pango_font_description_from_string (font_str);
  g_free (font_str);
  pango_layout_set_font_description (layout, font);
  pango_font_description_free (font);
  scm_remember_upto_here_1 (scm_layout);
  return SCM_UNSPECIFIED;
}

static SCM
scm_baka_layout_set_alignment_x (SCM scm_layout, SCM alignment)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  if (scm_is_eq (alignment, sym_left))
    pango_layout_set_alignment (layout, PANGO_ALIGN_LEFT);
  else if (scm_is_eq (alignment, sym_center))
    pango_layout_set_alignment (layout, PANGO_ALIGN_CENTER);
  else if (scm_is_eq (alignment, sym_right))
    pango_layout_set_alignment (layout, PANGO_ALIGN_RIGHT);
  // TODO: Warn about illegal values?
  scm_remember_upto_here_1 (scm_layout);
  return SCM_UNSPECIFIED;
}

static SCM
scm_baka_layout_set_spacing_x (SCM scm_layout, SCM scm_line_spacing, SCM scm_spacing)
{
  PangoLayout *layout = (PangoLayout *) scm_to_pointer (scm_layout);
  double line_spacing, spacing;
  line_spacing = scm_to_double (scm_line_spacing);
  spacing = scm_to_double (scm_spacing);
#if PANGO_VERSION_CHECK(1, 44, 0)
  pango_layout_set_line_spacing (layout, line_spacing);
#endif
  pango_layout_set_spacing (layout, spacing);
  scm_remember_upto_here_1 (scm_layout);
  return SCM_UNSPECIFIED;
}

static SCM
scm_baka_paint_x (SCM scm_texture,
                  SCM scm_layout,
                  SCM scm_options)
{
  baka_paint ((SDL_Texture *) scm_to_pointer (scm_texture),
              (PangoLayout *) scm_to_pointer (scm_layout),
              (BakaPaintOptions *) scm_to_pointer (scm_options));
  scm_remember_upto_here_2(scm_layout, scm_options);
  return SCM_UNSPECIFIED;
}

static SCM
scm_baka_add_font_dir_x (SCM dir)
{
  gchar *font_dir = scm_to_utf8_string (dir);
  gboolean ret = FcConfigAppFontAddDir (FcConfigGetCurrent (), font_dir);
  g_free (font_dir);
  return scm_from_bool (ret);
}

void
baka_init_scm_text ()
{
  sym_left = scm_from_utf8_symbol ("left");
  sym_center = scm_from_utf8_symbol ("center");
  sym_right = scm_from_utf8_symbol ("right");

  scm_c_define_gsubr ("%make-layout", 0, 0, 0, scm_baka_make_layout);
  scm_c_define_gsubr ("%paint!", 3, 0, 0, scm_baka_paint_x);
  scm_c_define_gsubr ("%layout-get-text", 1, 0, 0,
                      scm_baka_layout_get_text);
  scm_c_define_gsubr ("%layout-set-text!", 2, 0, 0,
                      scm_baka_layout_set_text_x);
  scm_c_define_gsubr ("%layout-get-attributes", 1, 0, 0,
                      scm_baka_layout_get_attributes);
  scm_c_define_gsubr ("%layout-set-attributes!", 2, 0, 0,
                      scm_baka_layout_set_attributes_x);
  scm_c_define_gsubr ("%layout-get-font", 1, 0, 0,
                      scm_baka_layout_get_font);
  scm_c_define_gsubr ("%layout-set-font!", 2, 0, 0,
                      scm_baka_layout_set_font_x);
  scm_c_define_gsubr ("%layout-get-alignment", 1, 0, 0,
                      scm_baka_layout_get_alignment);
  scm_c_define_gsubr ("%layout-set-alignment!", 2, 0, 0,
                      scm_baka_layout_set_alignment_x);
  scm_c_define_gsubr ("%layout-get-spacing", 1, 0, 0,
                      scm_baka_layout_get_spacing);
  scm_c_define_gsubr ("%layout-set-spacing!", 3, 0, 0,
                      scm_baka_layout_set_spacing_x);
  scm_c_define_gsubr ("add-font-dir!", 1, 0, 0, scm_baka_add_font_dir_x);
}
