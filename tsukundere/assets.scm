;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere assets)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 ftw)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module ((baka text) #:prefix baka:)
  #:use-module (tsukundere components textures)
  #:use-module (tsukundere sound)
  #:export (set-asset-path!
            add-font-dir!
            expand-asset-path
            (load-image/asset . load-image)
            (load-music/asset . load-music)
            (load-sound/asset . load-sound)
            load-assets load-images load-sounds load-music*))

(define *asset-root* (make-unbound-fluid))

;; Set @var{directory} as the directory in which assets are located.
(define (set-asset-path! directory)
  (fluid-set! *asset-root* (canonicalize-path directory)))

;; Expand @var{file} under the current asset path.
;; @quotation Note
;; It is usually not necessary to call this function explicitly.
;; The @code{(tsukundere)} module publicly exports the functions
;; @code{load-image}, @code{load-music} and @code{load-sound}, which already
;; expand it on their own.  Likewise, all functions derived from
;; @code{load-assets} also expand the asset path.
;; @end quotation
(define (expand-asset-path file)
  (cond
   ((absolute-file-name? file) file)
   ((fluid-bound? *asset-root*)
    (string-append (fluid-ref *asset-root*) "/" file))
   (else file)))

;; Expand @var{dir} using @code{expand-asset-path} and add it to the directories
;; scanned for fonts.
(define (add-font-dir! dir)
  (baka:add-font-dir! (expand-asset-path dir)))

;; Load assets from files in @var{directory}, whose names conform to the PEG
;; pattern @var{pattern} and convert them to something usable with
;; @var{converter}, finally storing them in an association list, whose
;; keys are formed according to @var{keys}.
;; @example
;; (use-modules (ice-9 peg) (tsukundere utils))
;; (define-peg-pattern id all (+ (or (range #\a #\z) (range #\0 #\9))))
;; (define-peg-pattern id+ all (+ (and id (? (or "/" "-")))))
;; (define-peg-pattern scm all (and id+ ".scm"))
;; (load-assets identity "/path/to/tsukundere" scm '(id))
;; @result{} ((scripts/doc/snarf . "/path/to/tsukundere/scripts/doc-snarf.scm")
;; (tsukundere/utils . "/path/to/tsukundere/tsukundere/utils.scm")
;; ...)
;; @end example
;; Note, that @code{search-for-pattern} is used to match @var{pattern} rather
;; @code{match-pattern}.  This is so that leading directories do not show up
;; in the match, but it also means, that load-assets might return more than
;; you're looking for.
(define (load-assets converter directory pattern keys)
  "Load assets conforming to PEG pattern PATTERN from DIRECTORY, converting
them to something usable using CONVERTER and associating them with KEYS."
  (file-system-fold
   (const #t)
   (lambda (file stat result)
     (or (and-let* ((%match (search-for-pattern pattern file))
                    (asset (converter file))
                    (tree (peg:tree %match))
                    (ctx (keyword-flatten keys tree))
                    (path (string-join
                           (filter-map
                            (lambda (x)
                              (and (pair? x)
                                   (member (first x) keys)
                                   (second x)))
                            ctx)
                           "/")))
           (acons (string->symbol path) asset result))
         result))
   (lambda (file stat result) result)
   (lambda (file stat result) result)
   (lambda (file stat result) result)
   (lambda (file stat errno result) result)
   '()
   (expand-asset-path directory)))

(define load-image/asset (compose load-image expand-asset-path))
(define load-sound/asset (compose load-sound expand-asset-path))
(define load-music/asset (compose load-music expand-asset-path))

;; Load images from files in @var{directory} matching the PEG pattern
;; @var{pattern} and associate them with @var{keys}.
(define (load-images directory pattern keys)
  (load-assets load-image directory pattern keys))

;; Load sounds from files in @var{directory} matching the PEG pattern
;; @var{pattern} and associate them with @var{keys}.
(define (load-sounds directory pattern keys)
  "Load sounds conforming to PEG pattern PATTERN from DIRECTORY, associating
them by KEYS."
  (load-assets load-sound directory pattern keys))

;; Load music from files in @var{directory} matching the PEG pattern
;; @var{pattern} and associate them with @var{keys}.
(define (load-music* directory pattern keys)
  "Load music conforming to PEG pattern PATTERN from DIRECTORY, associating
them by KEYS."
  (load-assets load-music directory pattern keys))
