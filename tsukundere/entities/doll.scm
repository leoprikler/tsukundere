;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere entities doll)
  #:use-module (ice-9 arrays)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (sdl2 render)
  #:use-module (sdl2 surface)
  #:use-module (tsukundere assets)
  #:use-module (tsukundere components)
  #:use-module (tsukundere entities)
  #:use-module (tsukundere entities plain)
  #:use-module ((tsukundere game internals) #:select (current-renderer))
  #:use-module (tsukundere math)
  #:use-module ((tsukundere script utils) #:select (say))
  #:export (load-doll doll->procedure doll-copy))

(define *doll-registry* (make-registry))
(define-component display-name *doll-registry* 'name)
(define-component parts *doll-registry* 'parts)
(define-component active-regions *doll-registry* 'active-regions)

(define (part-name part) (array-ref part 0))
(define (part-dimensions part) (array-ref part 1))
(define (part-offset part) (array-ref part 2))
(define (part-variants part) (array-ref part 3))

(define (render-doll doll)
  (let* ((doll-dimensions (part-dimensions (array-cell-ref (parts doll) 0)))
         (position ((anchor->procedure (anchor doll))
                   (position doll)
                   doll-dimensions))
         (texture (texture doll)))
    (array-slice-for-each-in-order
     1
     (lambda (part off)
       (let ((pos (array+ position (part-offset part)))
             (dim (part-dimensions part))
             (center (anchor:center (part-offset part) doll-dimensions)))
         ;; Anchoring procedures compute the top-left coordinate under the
         ;; assumption, that their anchor is at the given position.
         ;; ‘anchor:center’ therefore computes POS - (DIMENSIONS/2), which
         ;; is the inverse of the “center” we seek here: (DIMENSIONS/2) - POS.
         (array-map! center (cute * -1 <>) center)
         (render-copy (current-renderer)
                      texture
                      #:srcrect
                      (list (array-ref off 0) (array-ref off 1)
                            (array-ref dim 0) (array-ref dim 1))
                      #:dstrect
                      (list (array-ref pos 0) (array-ref pos 1)
                            (array-ref dim 0) (array-ref dim 1))
                      #:angle (angle doll)
                      #:center
                      (list (array-ref center 0) (array-ref center 1)))))
     (parts doll)
     (active-regions doll))))

(define (split-parts part-spec)
  (let ((parts (make-array *unspecified* (length part-spec) 4))
        (active-regions (make-typed-array 's32 0 (length part-spec) 2))
        (spec (list->vector part-spec)))
    (for-each
     (lambda (i)
       (match-let ((part (array-cell-ref parts i))
                   (region (array-cell-ref active-regions i))
                   ((name pos off dim variants) (array-ref spec i)))
         (array-copy! off region)
         (array-set! part name 0)
         (array-set! part dim 1)
         (array-set! part pos 2)
         (let ((varpos (array-copy variants)))
           (array-index-map! varpos
                             (lambda (i j)
                               (s32vector (+ (array-ref off 0)
                                             (* (array-ref dim 0) j))
                                          (+ (array-ref off 1)
                                             (* (array-ref dim 1) i)))))
           (array-set! part
                       (map cons
                            (array->list (array-contents variants))
                            (array->list (array-contents varpos)))
                       3))))
     (iota (length part-spec)))
    (values parts active-regions)))

(define (doll-set! doll key value)
  (cond
   ((memq key '(position angle anchor))
    (entity-set! *registry* doll key value))
   (else
    (array-slice-for-each
     1
     (lambda (part region)
       (when (eq? (part-name part) key)
         (and=> (assq-ref (part-variants part) value)
                (cut array-copy! <> region))))
     (parts doll) (active-regions doll)))))

;; Return a procedure that can be called
;; @itemize
;; @item without arguments to return the underlying entity,
;; @item with a string and optional arguments, to use the doll as a speaker,
;; @item with @code{#:part value ...} to set the doll's parts to specific
;; variants.
;; @end itemize
(define (doll->procedure doll)
  (match-lambda*
    (() doll)
    (((? keyword? field) value . rest)
     (doll-set! doll (keyword->symbol field) value)
     (apply (doll->procedure doll) rest))
    (((? string? text) . args)
     (apply say (display-name doll) text args))))

;; Load a doll from @var{texture} using @var{parts}.
;; @var{parts} is a list of elements structured as
;; @code{(name position offset dimensions variants)},
;; where @var{name} labels a part, @var{position} is the relative offset of
;; that part to the top left corner of the doll, @var{offset} is the offset
;; at which @var{variants} are drawn in the texture, @var{dimensions} are the
;; dimensions of a single and @var{variants} describes which variants of a
;; part exist.
;;
;; For instance, a doll that has four different arm textures arranged in a
;; square, offset in the texture at @code{#s32(420 0)} and rendered at
;; @code{#s32(100 50)} with dimensions of @code{s32(42 42)}, would be described
;; as @code{(arm #s32(100 50) #s32(420 0) #s32(42 42) #2((a b) (c d)))}.
;;
;; Different parts can have the same name, which will cause Tsukundere to apply
;; the same variant to all same-named parts.  If a variant only exists in some
;; parts or variants are shuffled around between parts, Tsukundere takes care
;; to apply only the correct and existing variants.
;;
;; @var{texture} can alternatively be a file name or an SDL surface, in which
;; they will be loaded/converted to a texture.
(define* (load-doll texture parts
                    #:key display-name
                    (position #s32(0 0))
                    (angle 0)
                    (anchor 'center))
  (let ((doll (make-entity))
        (parts regions (split-parts parts)))
    (entity-set! *registry* doll 'position position)
    (entity-set! *registry* doll 'angle angle)
    (entity-set! *registry* doll 'anchor anchor)
    (entity-set! *registry* doll 'render render-doll)
    (entity-set! *registry* doll 'texture
                 (cond
                  ((string? texture) (load-image texture))
                  ((surface? texture)
                   (surface->texture (current-renderer) texture))
                  (else texture)))

    (entity-set! *doll-registry* doll 'name display-name)
    (entity-set! *doll-registry* doll 'parts parts)
    (entity-set! *doll-registry* doll 'active-regions regions)
    doll))

;; Create a new copy of @var{doll} with display name @var{display-name}.
;; Only components relevant to dolls will be copied to the new copy.
;; To copy further components, use @code{entity-copy!}.
(define* (doll-copy doll #:key display-name)
  (let ((copy (make-entity)))
    (entity-set! *doll-registry* copy
                 'name
                 (or display-name (entity-get *doll-registry* doll 'name)))
    (entity-copy! copy doll
                  `((,position . ,array-copy)
                    ,angle
                    ,anchor
                    ,render-function
                    ,texture
                    ,parts
                    (,active-regions . ,array-copy)))
    copy))
