;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere cli make-shell-script)
  #:use-module (ice-9 format)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-26)
  #:use-module (tsukundere cli)
  #:use-module (tsukundere i18n)
  #:export (*options* *values* *help* make-shell-script))

(define *options*
  (cons* (option '(#\L "load-path") #t #f (prepend 'load-path))
         (option '("load-compiled-path") #t #f (prepend 'load-compiled-path))
         (option '(#\A "asset-dir") #t #f (reset 'asset-dir))
         (option '(#\m "module") #t #f
                 (reset 'module
                        (compose (cute map string->symbol <>)
                                 (cute string-split <> #\space))))
         (option '("localedir") #t #f (reset 'localedir))
         (option '(#\o "output") #t #f
                 (lambda (opt name arg result)
                   (if (assoc-ref result 'output)
                       (fail "`--output' can not be specified more than once")
                       (acons 'output arg result))))
         (option '("shell") #t #f (reset 'shell))
         (option '("tsukundere") #t #f (reset 'tsukundere))
         base-options))

(define *values*
  '())

(define (*help*)
  (T_ "Usage: tsukundere make-shell-script [OPTION]...
Construct a shell script wrapper to run a game using Tsukundere.

  -h, --help                 print this help message
  -o, --output=FILE          output to FILE instead of the current output port
  --shell=SHELL              use SHELL as shell instead of `/bin/sh'
  --tsukudere=TSUKUNDERE     use TSUKUNDERE as the tsukundere command
                             instead of `tsukundere'

This command additionally takes all the options of `tsukundere run' and makes
sure, that `tsukundere run' is invoked with the same arguments.
There are only two exceptions to this, which are noted below.

  -L, --load-path=DIR        add dir to TSUKUNDERE_LOAD_PATH *prior to*
                             invoking `tsukundere run'
  --load-compiled-path=DIR   likewise, but for TSUKUNDERE_LOAD_COMPILED_PATH"))


(define* (format-sh port #:key
                    module
                    asset-dir
                    localedir
                    (shell "/bin/sh")
                    (tsukundere-command "tsukundere")
                    (load-path '())
                    (load-compiled-path '()))
  (format port "\
#!~a
~@[export TSUKUNDERE_LOAD_PATH=~s~%~]~
~@[export TSUKUNDERE_LOAD_COMPILED_PATH=~s~%~]~
~a run ~@[--asset-dir=~s ~]~@[--localedir=~s ~]--module=\"~{~a~^ ~}\"
"

          shell
          (and load-path (string-join load-path ":"))
          (and load-compiled-path (string-join load-compiled-path ":"))
          tsukundere-command asset-dir localedir module))

(define* (make-shell-script #:key help?
                            output
                            load-path load-compiled-path
                            (shell "/bin/sh") (tsukundere "tsukundere")
                            module asset-dir localedir)
  (when help? (show-help "make-shell-script"))
  (unless module
    (fail "no module given"))

  (let ((do-format
         (cute format-sh <>
               #:load-path load-path
               #:load-compiled-path load-compiled-path
               #:shell shell
               #:tsukundere-command tsukundere
               #:module module
               #:asset-dir asset-dir
               #:localedir localedir)))
    (cond
     ((string? output) (call-with-output-file output do-format))
     ((output-port? output) (do-format output))
     (else (do-format (current-output-port))))))
