;; Copyright © 2020 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere preferences)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-71)
  #:use-module (tsukundere utils)
  #:export (make-preference
            preference->procedure

            load-preferences
            save-preferences

            add-preference-hook!))

(define *preferences* (make-hash-table))

(define-record-type <preference>
  (%make-preference values validate? hook)
  preference?
  (values preference-values set-preference-values!)
  (validate? preference-validate?)
  (hook preference-hook))

;; Set the value of @var{preference} as given by @var{source} to @var{value}
(define* (set-preference! preference value #:optional source)
  (when ((preference-validate? preference) value)
    (set-preference-values! preference
                            (assoc-set! (preference-values preference)
                                        source value))
    (run-hook (preference-hook preference)
              (cdar (preference-values preference)))))

(define (preference->procedure preference)
  (make-procedure-with-setter
   (lambda ()
     (cdar (preference-values preference)))
   (case-lambda
     ((value) (set-preference! preference value #f))
     ((source value) (set-preference! preference value source)))))

;; Return a preference named @var{name}.  If no such preference exists, create
;; one using @var{default} as default value and @var{validate?} to check whether
;; values passed in are valid for that preference.
;; If @var{documentation} is given set it as object documentation for the
;; returned preference.
(define* (make-preference name default
                          #:key (validate? (const #t))
                          documentation)
  (unless (symbol? name)
    (error "not a valid preference key: ~s" name))
  (let* ((%newval (%make-preference `((*source* . ,default))
                                    validate?
                                    (make-hook 1)))
         (handle (hashq-create-handle! *preferences* name %newval))
         (pref (cdr handle)))
    (when documentation
      (set-object-property! %newval 'documentation documentation))
    pref))

(define (%load-preferences file)
  (call-with-input-file file
    (lambda (port)
      (for-each
       (lambda (kv)
         (let* ((key value (car+cdr kv))
                (pref (hashq-ref *preferences* key)))
           (when pref
             (set-preference! pref value file))))
       (read port)))))

(define* (load-preferences #:optional game)
  (let ((file (tsukundere-config-file
               (if game
                   (string-append game ".preferences")
                   "preferences"))))
    (when (file-exists? file)
      (%load-preferences file))))

(define (%game-preferences preferences file)
  (hash-fold
   (lambda (key pref seed)
     (if (null?
          (lset-intersection equal?
                             (list '*game* file)
                             (map car (preference-values pref))))
         seed
         (acons key (cdar (preference-values pref))
                seed)))
   '() preferences))

(define (%all-preferences preferences . ignored)
  (hash-fold
   (lambda (key pref seed)
     (acons key (cdar (preference-values pref))
            seed))
   '() preferences))

(define (%save-preferences file select)
  (let ((selected (select *preferences* file)))
    (unless (null? selected)
      (call-with-output-file file
        (lambda (port)
          (pretty-print selected port))))))

;; Add @var{callback} as a hook to be called when the value of @var{preference}
;; is set.  @var{callback} will be called with a single argument, which is the
;; new value of @var{preference}.  This value need not be different from the
;; old value.
(define (add-preference-hook! preference callback)
  (add-hook! (preference-hook preference) callback))

(define* (save-preferences #:optional game (when? #t))
  (let ((file (tsukundere-config-file
               (if game
                   (string-append game ".preferences")
                   "preferences")))
        (select (if game %game-preferences %all-preferences)))
    (mkdir-p (tsukundere-config-dir))
    (match (list when? (file-exists? file))
      ((#t _) (%save-preferences file select))
      (('if-exists? #t) (%save-preferences file select))
      (('unless-exists? #f) (%save-preferences file select))
      ((_ _) *unspecified*))))
