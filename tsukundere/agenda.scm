;; Copyright © 2020 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere agenda)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-9)
  #:export (make-agenda
            agenda-time

            update-agenda!
            reset-agenda! reset-agenda/keep-plans!
            finish-agenda!
            agenda-filled?

            schedule-at!
            schedule-after!
            schedule-plan! drop-plan!))

(define-record-type <agenda>
  (%make-agenda time tasks plans)
  agenda?
  (time agenda-time set-agenda-time!)
  (tasks agenda-tasks set-agenda-tasks!)
  (plans agenda-plans set-agenda-plans!))

;; Allocate a new agenda.
(define (make-agenda)
  (%make-agenda 0 '() '()))

(define (pop-task? agenda)
  (let ((tasks (agenda-tasks agenda)))
    (and (not (null? tasks))
         (<= (caar tasks) (agenda-time agenda))
         (let ((thunk (cdar tasks)))
           (set-agenda-tasks! agenda (cdr tasks))
           thunk))))

;; Update @var{agenda} by @var{dt}, running any tasks that might have been
;; scheduled in the meantime.
(define (update-agenda! agenda dt)
  (set-agenda-time! agenda (+ (agenda-time agenda) dt))
  (let loop ((thunk (pop-task? agenda)))
    (when thunk
      (thunk)
      (loop (pop-task? agenda))))
  (for-each
   (lambda (plan) (plan dt))
   (agenda-plans agenda)))

;; Clear @var{agenda} and reset the internal time.
(define (reset-agenda! agenda)
  (set-agenda-plans! agenda '())
  (set-agenda-tasks! agenda '())
  (set-agenda-time! agenda 0))

;; Clear @var{agenda} and reset the internal time, but keep all plans scheduled.
(define (reset-agenda/keep-plans! agenda)
  (set-agenda-tasks! agenda '())
  (set-agenda-time! agenda 0))

;; Run @var{agenda} until all tasks are completed.  This may take forever.
(define (finish-agenda! agenda)
  "Run all agenda tasks until completion. This may take forever."
  (while (not (null? (agenda-tasks agenda)))
    (update-agenda! agenda 1)))

;; Return @code{#t} if @var{agenda} has a task, that is not yet run.
(define (agenda-filled? agenda)
  (not (null? (agenda-tasks agenda))))

(define (%insert-task tasks task)
  (let loop ((previous '())
             (next tasks))
    (cond
     ((null? next)
      (reverse! previous (list task)))
     ((<= (caar tasks) (car task))
      (loop (cons (car tasks) previous) (cdr tasks)))
     (else
      (reverse! previous (cons task next))))))

;; Schedule @var{thunk} to be executed at @var{time} in @var{agenda}.
(define (schedule-at! agenda time thunk)
  (when (< time (agenda-time agenda))
    (error "cannot schedule task in the past: ~a < ~a"
           time (agenda-time agenda)))
  (set-agenda-tasks! agenda
                     (%insert-task (agenda-tasks agenda) (cons time thunk))))

;; Schedule @var{thunk} to be executed after @var{time} steps have elapsed
;; in @var{agenda}.
(define (schedule-after! agenda time thunk)
  (schedule-at! agenda (+ (agenda-time agenda) time) thunk))

;; Schedule @var{plan} on every update of @var{agenda}.
(define (schedule-plan! agenda plan)
  (set-agenda-plans! agenda (cons plan (agenda-plans agenda))))

;; Stop scheduling @var{plan} in @var{agenda}.
(define (drop-plan! agenda plan)
  (set-agenda-plans! agenda (delete plan (agenda-plans agenda))))
